package internal

import (
	"database/sql"
	e "file-converter/helpers"
	"fmt"

	conf "github.com/tkanos/gonfig"
)

//OpenDateBase ...
func OpenDateBase() (db *sql.DB, err error) {
	configuration := Configuration{}
	if err = conf.GetConf("./conf/db_conf.json", &configuration); err == nil {
		connectionString := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=%s",
			configuration.Host, configuration.User, configuration.Pass, configuration.DbName, configuration.Mode)
		if db, err = sql.Open("postgres", connectionString); err != nil {
			e.CheckErr(&err)
		}
	}
	return
}

//ReadFiles ...
func ReadFiles(db *sql.DB) (files *[]File, err error) {
	if rows, err := db.Query(`select * from files where is_convert = false`); err == nil {
		defer rows.Close()
		var files []File
		for rows.Next() {
			var file File
			if err = rows.Scan(&file.ID, &file.InputhFileName, &file.OutputFileName, &file.IsConvert); err == nil {
				files = append(files, file)
			} else {
				e.CheckErr(&err)
			}
		}
		return &files, nil
	}
	e.CheckErr(&err)
	return
}

//UpdateFile ...
func UpdateFile(db *sql.DB, id int) (err error) {
	if _, err := db.Exec(fmt.Sprintf(`UPDATE public.files
				SET is_convert=true
				WHERE id=%s;`, fmt.Sprint(id))); err != nil {
		e.CheckErr(&err)
	}
	return
}
