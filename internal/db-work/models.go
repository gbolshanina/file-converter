package internal

//Configuration ...
type Configuration struct {
	User   string `json:"user"`
	Pass   string `json:"pass"`
	DbName string `json:"dbName"`
	Host   string `json:"host"`
	Mode   string `json:"mode"`
}

//File ...
type File struct {
	ID             int
	InputhFileName string
	OutputFileName string
	IsConvert      bool
}
