package internal

import (
	"bufio"
	"errors"
	e "file-converter/helpers"
	"fmt"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

func getRatio(mSize, fileSize int64) float64 {
	return (float64(mSize) / float64(fileSize)) * 100000
}

//ConvertFile ...
func ConvertFile(inputFileName, outFileName string) (err error) {
	file, err := os.Open(inputFileName)
	if err != nil {
		e.CheckErr(&err)
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		e.CheckErr(&err)
	}
	args := []string{"-i", inputFileName, CheckDuplicateFileName(outFileName), "-y"}
	if err = RunCMD("ffmpeg", args, true, stat.Size()); err != nil {
		e.CheckErr(&err)
	}
	return
}

// RunCMD выполняет команду в терминале
func RunCMD(path string, args []string, debug bool, fileSize int64) (err error) {

	cmd := exec.Command(path, args...)

	stderr, err := cmd.StderrPipe()
	if err != nil {
		e.CheckErr(&err)
	}
	cmd.Start()

	scanner := bufio.NewScanner(stderr)
	scanner.Split(bufio.ScanWords)
	for scanner.Scan() {
		m := scanner.Text()
		if strings.Contains(m, "kB") {
			s := strings.Split(m, "k")
			if len(s) > 1 {
				if mSize, err := strconv.ParseInt(s[0], 10, 64); err == nil {
					fmt.Println("!:   ", math.Round(getRatio(mSize, fileSize)), "%")
				} else {
					e.CheckErr(&err)
				}
			} else {
				err := errors.New("Ошибка размера файла")
				e.CheckErr(&err)
			}
		}
	}
	cmd.Wait()
	return
}

// CheckDuplicateFileName проверяет есть ли файлы с таким именем и сколько их. формирует новое имя файла по количеству уже имеющихся копий.
func CheckDuplicateFileName(fileName string) (fullName string) {
	fileNumber := 0
	fullName = fileName
	files, err := filepath.Glob("*")
	if err != nil {
		e.CheckErr(&err)
	} else {
		for _, file := range files {
			if strings.Contains(file, fileName) {
				fileNumber++
			}
		}
	}
	if fileNumber != 0 {
		str := strings.Split(fileName, ".")
		fullName = fmt.Sprintf("%s(%s).%s", str[0], fmt.Sprint(fileNumber), str[1])

		flag := true
		for flag {
			cFileNumber := fileNumber
			for _, file := range files {
				if strings.Contains(file, fullName) {
					fileNumber++
				}
			}
			s := strings.Split(fileName, ".")
			fullName = fmt.Sprintf("%s(%s).%s", s[0], fmt.Sprint(fileNumber), s[1])
			if cFileNumber == fileNumber {
				flag = false
			}
		}
	}
	return fullName
}
