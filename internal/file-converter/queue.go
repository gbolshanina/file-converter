package internal

import (
	e "file-converter/helpers"
	"fmt"
)

// NewWorker ...
func NewWorker(id int, workerPool chan chan Job) Worker {
	return Worker{
		id:         id,
		jobQueue:   make(chan Job),
		workerPool: workerPool,
		quitChan:   make(chan bool),
	}
}

func (w Worker) start() {
	go func() {
		for {
			w.workerPool <- w.jobQueue

			select {
			case job := <-w.jobQueue:
				fmt.Printf("worker%d started\n", w.id)
				if err := ConvertFile(job.InputFileName, job.OutFileName); err != nil {
					e.CheckErr(&err)
				}

				defer job.Wg.Done()
				fmt.Printf("worker%d completed\n", w.id)
			case <-w.quitChan:
				fmt.Printf("worker%d stopping\n", w.id)
				return
			}
		}
	}()
}

func (w Worker) stop() {
	go func() {
		w.quitChan <- true
	}()
}

// NewDispatcher ...
func NewDispatcher(jobQueue chan Job, maxWorkers int) *Dispatcher {
	workerPool := make(chan chan Job, maxWorkers)

	return &Dispatcher{
		jobQueue:   jobQueue,
		maxWorkers: maxWorkers,
		workerPool: workerPool,
	}
}

//Run ...
func (d *Dispatcher) Run() {
	for i := 0; i < d.maxWorkers; i++ {
		worker := NewWorker(i+1, d.workerPool)
		worker.start()
	}

	go d.dispatch()
}

func (d *Dispatcher) dispatch() {
	for {
		select {
		case job := <-d.jobQueue:
			go func() {
				fmt.Printf("fetching new worker to queue\n")
				workerJobQueue := <-d.workerPool
				fmt.Printf("worker adding to queue\n")
				workerJobQueue <- job
			}()
		}
	}
}
