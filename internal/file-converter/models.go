package internal

import "sync"

// Job ...
type Job struct {
	InputFileName string
	OutFileName   string
	Wg            *sync.WaitGroup
}

//Dispatcher ...
type Dispatcher struct {
	workerPool chan chan Job
	maxWorkers int
	jobQueue   chan Job
}

//Worker ...
type Worker struct {
	id         int
	jobQueue   chan Job
	workerPool chan chan Job
	quitChan   chan bool
}
