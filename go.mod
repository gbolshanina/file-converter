module file-converter

require (
	github.com/astaxie/beego v1.10.1
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/lib/pq v1.0.0
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	gopkg.in/yaml.v2 v2.2.1 // indirect
)
