package main

import (
	"database/sql"
	h "file-converter/cmd/file-converter/handlers"
	e "file-converter/helpers"
	d "file-converter/internal/db-work"
	i "file-converter/internal/file-converter"
	"fmt"
	"net/http"
	"sync"
	"time"

	_ "github.com/lib/pq"

	"github.com/gorilla/mux"
)

func main() {
	var (
		maxWorkers    = 1
		maxQueueSize  = 100
		inputFileName = "inputfile.mp4"
		outFileName   = "outfile.avi"
	)

	jobQueue := make(chan i.Job, maxQueueSize)
	dispatcher := i.NewDispatcher(jobQueue, maxWorkers)
	dispatcher.Run()

	go func() {
		db, err := d.OpenDateBase()
		defer db.Close()
		if err == nil {
			auntoFileConvert(10, db, maxWorkers)
		} else {
			e.CheckErr(&err)
		}
	}()

	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		h.RequestHandler(w, r, jobQueue, maxWorkers, inputFileName, outFileName)
	})
	http.ListenAndServe(":8001", r)
}

func auntoFileConvert(n time.Duration, db *sql.DB, maxWorkers int) {
	for range time.Tick(n * time.Second) {
		str := "Check new files......"
		fmt.Println(str)
		wg := &sync.WaitGroup{}
		if files, err := d.ReadFiles(db); err == nil {
			fmt.Println("New files: ", len(*files))
			for _, file := range *files {
				fmt.Println("Convet file: ", file.ID)
				wg.Add(1)
				go func(file d.File) {
					defer wg.Done()
					d.UpdateFile(db, file.ID)
					i.ConvertFile(file.InputhFileName, file.OutputFileName)
				}(file)
				wg.Wait()
			}
		}
		str = "done"
		fmt.Println(str)
	}
}
