package handlers

import (
	i "file-converter/internal/file-converter"
	"net/http"
	"sync"
)

//RequestHandler ...
func RequestHandler(w http.ResponseWriter, r *http.Request, jobQueue chan i.Job, maxWorkers int, inputFileName, outFileName string) {
	var wg sync.WaitGroup
	wg.Add(maxWorkers)

	job := i.Job{InputFileName: inputFileName, OutFileName: outFileName, Wg: &wg}
	jobQueue <- job

	wg.Wait()
	w.WriteHeader(http.StatusCreated)
}

//ConvertFile ...
func ConvertFile(jobQueue chan i.Job, maxWorkers int, inputFileName, outFileName string) {
	var wg sync.WaitGroup
	wg.Add(maxWorkers)

	job := i.Job{InputFileName: inputFileName, OutFileName: outFileName, Wg: &wg}
	jobQueue <- job

	wg.Wait()
}
