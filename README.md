## Quick Start

#### Run 

	go run main.go

&emsp;При обращении к http://localhost:8001/ в очредь добавляется файл на конвертацию. Вся информация о процессе работы выводится в консоль. При запуске появляются логи ошибок как пример.

## Задание 3

**Добавить таблицу в postgres**

```sh
CREATE DATABASE mts_db_test OWNER = postgres;
GRANT ALL PRIVILEGES ON database mts_db_test TO postgres;

CREATE TABLE public.files (
	id serial NOT NULL,
	input_path varchar(255) NOT NULL,
	output_path varchar(255) NOT NULL,
	is_convert bool null default false,
	CONSTRAINT files_pk PRIMARY KEY (id)
	)
WITH (
	OIDS=FALSE
) ;
```

**Добавляем данные в таблицу**
&emsp;Данные можно добавить все сразу. Они будут сразу отправленые на конвертацию при запуске програмы (каждые 10 секунд). Отконвертированным файлам значение is_convert выставляется в true. Его можно менять в базе. Или же можно добавлять файлы во время выполнения программы.

```sh
INSERT INTO public.files
(input_path, output_path, is_convert)
VALUES('inputfile-1.mp4', 'outfile-1.avi', false);

INSERT INTO public.files
(input_path, output_path, is_convert)
VALUES('inputfile-2.mp4', 'outfile-2.avi', false);

INSERT INTO public.files
(input_path, output_path, is_convert)
VALUES('inputfile-3.mp4', 'outfile-3.avi', false);
```
&emsp;Зпрос на добавление в очередь таже же работает по лоокалхосту, но будет выполнятся паралелльно с запросом к базе.

