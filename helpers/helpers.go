package helpers

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/astaxie/beego/logs"
)

func exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func createDir(path string) error {
	if exists(path) == false {
		err := os.MkdirAll(path, os.ModePerm)
		if err != nil {
			return err
		}
	}
	return nil
}

// CheckErr ...
func CheckErr(err *error) {
	var errDir error
	errDir = createDir("logs/")
	if errDir == nil {
		t := time.Now()
		timeToString := t.Format(time.RFC3339)
		s := strings.Split(timeToString, "T")
		timeToString = s[0]
		fileName := fmt.Sprintf("%s%s%s", "logs/error_", timeToString, ".log")

		if _, err := os.Stat(fileName); err != nil {
			if os.IsNotExist(err) {
				os.Create(fileName)
			}
		}

		log := logs.NewLogger(10000)
		log.EnableFuncCallDepth(true)
		log.SetLogFuncCallDepth(3)

		fileNameMod := fmt.Sprintf("%s%s%s", `{"filename":"`, fileName, `"}`)
		log.SetLogger("file", fileNameMod)
		var errL = *err
		log.Error("Error: ", errL.Error())
	}
}
